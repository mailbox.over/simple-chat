import { takeEvery, all, fork, call, put } from 'redux-saga/effects';
import { updateMessages } from '../actions/ChatActions';
import { FETCH_MESSAGES } from '../constants';
import Api from '../api/Api';

function* fetchMessagesCb() {
  const api = new Api();
  const r = yield call(api.getMessages);
  yield put(updateMessages(r));
}

function* fetchMessagesSaga() {
  yield takeEvery(FETCH_MESSAGES, fetchMessagesCb);
}

function* rootSaga() {
  yield all([fork(fetchMessagesSaga)]);
}

export { rootSaga as default };
