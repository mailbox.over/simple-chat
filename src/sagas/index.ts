import { all } from 'redux-saga/effects';
import messageSagas from './Message';

function* rootSaga() {
  yield all([messageSagas()]);
}

export { rootSaga as default };
