import { FETCH_MESSAGES, SEND_MESSAGE, UPDATE_MESSAGES } from '../constants';
import Message from '../entities/Message';

export const fetchMessages = () => {
  return {
    type: FETCH_MESSAGES,
  };
};

export const updateMessages = (messages: Message[]) => {
  return {
    type: UPDATE_MESSAGES,
    payload: messages,
  };
};

export const sendMessage = (message: Message) => {
  return {
    type: SEND_MESSAGE,
    payload: message,
  };
};
