import Message from './Message';

export default interface ReduxState {
  messages: Message[];
}
