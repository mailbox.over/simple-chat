import {
  UPDATE_MESSAGES,
  FETCH_MESSAGES,
  RECEIVE_MESSAGE,
  SEND_MESSAGE,
} from '../constants/ActionTypes';
import Message from './Message';

interface FetchMessagesAction {
  type: typeof FETCH_MESSAGES;
}

interface UpdateMessagesAction {
  type: typeof UPDATE_MESSAGES;
  payload: Message[];
}

interface SendMessagesAction {
  type: typeof SEND_MESSAGE;
  payload: Message;
}

export type ChatActionTypes =
  | FetchMessagesAction
  | UpdateMessagesAction
  | SendMessagesAction;
