export default interface Message {
  id: string;
  avatar: string;
  name: string;
  body: string;
  time: number;
}
