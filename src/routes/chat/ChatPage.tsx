import React, { useEffect } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { fetchMessages } from '../../actions/ChatActions';
import ReduxState from '../../entities/ReduxState';
import Chat from '../../components/Chat/Chat';

const ChatPage = () => {
  const dispatch = useDispatch();

  const messages = useSelector(
    (state: ReduxState) => state.messages,
    shallowEqual
  );

  useEffect(() => {
    dispatch(fetchMessages());
  }, []);

  return (
    <>
      <Chat messages={messages} />
    </>
  );
};

export default ChatPage;
