import { combineReducers } from 'redux';
import ChatReducers from './ChatReducers';

const reducers = combineReducers<any>({
  messages: ChatReducers,
});

const rootReducer = (state: any, action: any) => {
  return reducers(state, action);
};

export default rootReducer;
