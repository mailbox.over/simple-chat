import { SEND_MESSAGE, UPDATE_MESSAGES } from '../constants';
import { ChatActionTypes } from '../entities/ActionTypes';
import Message from '../entities/Message';

const INIT_STATE: Message[] = [];

export default (state = INIT_STATE, action: ChatActionTypes) => {
  switch (action.type) {
    case UPDATE_MESSAGES: {
      return action.payload;
    }
    case SEND_MESSAGE: {
      return state.concat(action.payload);
    }
    default:
      return state;
  }
};
