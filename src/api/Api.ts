import Message from '../entities/Message';

export default class Api {
  getMessages = async (): Promise<Message[]> => {
    const url = `/data/messages.json`;
    const r = await fetch(url);
    if (r.ok) {
      return r.json();
    }
    throw new Error('Network error');
  };
}
