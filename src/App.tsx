import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ChatPage from './routes/chat';
import configureStore from './store';
import { Provider } from 'react-redux';

const store = configureStore();

const App: React.FC = () => {
  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/chat" component={ChatPage} />
            <Route path="/history" component={ChatPage} />
          </Switch>
        </Router>
      </Provider>
    </div>
  );
};

export default App;
