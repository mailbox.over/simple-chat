import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import Message from '../../entities/Message';
import MessageItem from './MessageItem';
import MessageForm from './MessageForm';

const MessagesWrapper = styled.div`
  height: 100%;
`;

const ChatHeader = styled(Grid)`
  height: 100px;
  flex-basis: 100px !important;
`;

const ChatWrapper = styled(Grid)`
  height: 100%;
`;

const ChatExpander = styled(Grid)`
  flex-grow: 1;
  overflow: auto;
  background-color: #f4f4f7;
  padding: 1rem;
`;

interface Props {
  messages: Message[];
}

const Chat = ({ messages }: Props) => {
  const ref = React.createRef<HTMLDivElement>();
  useEffect(() => {
    if (ref && ref.current) {
      const el = ref.current;
      const last = el.lastElementChild;
      if (last) {
        last.scrollIntoView({
          behavior: 'smooth',
        });
      }
    }
  }, [messages]);
  return (
    <ChatWrapper container direction="column" wrap="nowrap">
      <ChatHeader item xs={12}>
        <h1>Simple Chat</h1>
      </ChatHeader>
      <ChatExpander item xs={12}>
        <MessagesWrapper ref={ref} id="message-wrapper">
          {Array.isArray(messages) &&
            messages.map((m: Message) => (
              <MessageItem key={m.id} message={m} />
            ))}
        </MessagesWrapper>
      </ChatExpander>
      <MessageForm />
    </ChatWrapper>
  );
};

export default Chat;
