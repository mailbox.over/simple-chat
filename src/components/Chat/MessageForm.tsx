import React, { SyntheticEvent, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { IoIosSend as SendIcon } from 'react-icons/all';
import styled from 'styled-components';
import IconButton from '@material-ui/core/IconButton';
import { useDispatch } from 'react-redux';
import { TextField } from '@material-ui/core';
import { sendMessage } from '../../actions/ChatActions';
import Message from '../../entities/Message';
import moment from 'moment';

const SendButton = styled(IconButton)`
  background-color: #345acb !important;
  color: #fff !important;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 1rem;
  box-sizing: border-box;
`;

const TextAreaContainer = styled(Grid)`
  flex-basis: auto !important;
`;

const TextAreaWrapper = styled(Grid)`
  box-sizing: border-box;
  padding: 1rem;
  display: flex;
  align-items: flex-end;
  height: 100%;
`;

const MessageForm = () => {
  const dispatch = useDispatch();

  const [newMessage, setNewMessage] = useState('');

  const handleSubmit = (e: SyntheticEvent) => {
    const message: Message = {
      id: Math.random().toString(),
      avatar: 'https://via.placeholder.com/150',
      name: '',
      body: newMessage,
      time: Math.floor(moment().unix() / 1000),
    };
    dispatch(sendMessage(message));
    e.preventDefault();
  };

  return (
    <form onSubmit={handleSubmit} method="post" style={{ width: '100%' }}>
      <TextAreaContainer item xs={12}>
        <Grid container direction="row" style={{ height: '100%' }}>
          <Grid item xs={10}>
            <TextAreaWrapper>
              <TextField
                onChange={(e: { target: { value: string } }) =>
                  setNewMessage(e.target.value)
                }
                multiline
                required
                fullWidth
              />
            </TextAreaWrapper>
          </Grid>
          <Grid item xs={2}>
            <ButtonWrapper>
              <SendButton type="submit">
                <SendIcon />
              </SendButton>
            </ButtonWrapper>
          </Grid>
        </Grid>
      </TextAreaContainer>
    </form>
  );
};

export default MessageForm;
