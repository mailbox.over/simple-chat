import React from 'react';
import Message from '../../entities/Message';
import Grid from '@material-ui/core/Grid/Grid';
import { Avatar } from '@material-ui/core';
import styled from 'styled-components';
import moment from 'moment';

const ChatAvatar = styled(Avatar)`
  width: 100px !important;
  height: auto !important;
  max-width: 100% !important;
`;

const MessageRow = styled.div`
  padding: 1rem;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;

const MessageBodyWrapper = styled.div`
  text-align: left;
`;

const MessageTimeWrapper = styled.div`
  text-align: right;
`;

interface Props {
  message: Message;
}

const MessageItem = ({ message }: Props) => {
  const { id, avatar, body, name, time } = message;
  const timeFormatted = moment.unix(time).format('DD.MM.YY hh:mm:ss');
  return (
    <MessageRow>
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <ChatAvatar src={avatar} />
        </Grid>
        <Grid item xs={7}>
          <MessageBodyWrapper>{body}</MessageBodyWrapper>
        </Grid>
        <Grid item xs={3}>
          <MessageTimeWrapper>{timeFormatted}</MessageTimeWrapper>
        </Grid>
      </Grid>
    </MessageRow>
  );
};

export default MessageItem;
