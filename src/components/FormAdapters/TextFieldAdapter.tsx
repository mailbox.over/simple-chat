import * as React from 'react';
import TextField from '@material-ui/core/TextField';

const TextFieldAdapter: React.FC<any> = ({
  input: { name, onChange, value, ...restInput },
  meta,
  suffix,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  return (
    <TextField
      {...rest}
      name={name}
      helperText={showError ? meta.error || meta.submitError : undefined}
      error={showError}
      inputProps={{ ...restInput, suffix }}
      onChange={onChange}
      value={value}
      variant="outlined"
      margin={rest.margin || 'dense'}
    />
  );
};

export default TextFieldAdapter;
